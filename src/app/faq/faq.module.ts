import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FaqComponent } from './faq/faq.component';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [FaqComponent],
  imports: [
    CommonModule,
    RouterModule.forChild([
      { path: 'faq', component: FaqComponent },
    ])
  ]
})
export class FaqModule { }
